const db = require('../../db');
const Bicicleta = require('../../models/Bicicleta.model');
const Bicicletas = require('../../models/Bicicletas.model');

describe('test Bicicletas Model', () => {
    
    const bicicleta = Bicicleta.create({color: 'rojo', modelo: 'rojo', latitud: -34.897424, longitud: -34.5664542});

    afterEach(async (done) => {
        await Bicicletas.empty()
        done();
    });
    
    it('test all method', async(done) => {
        const size = await Bicicletas.size();
        expect(size).toBe(0);
        done();
    });
    
    it('test add method', async (done) => {
        const _bicicleta = await Bicicletas.add(bicicleta);
        const size = await Bicicletas.size();
        expect(size).toBe(1);
        done();
    });

    it('test find method', async (done) => {
        const Bicicleta = await Bicicletas.add(bicicleta);
        const _bicicleta = await Bicicletas.show(Bicicleta.id);
        const size = await Bicicletas.size();
        expect(size).toBe(1);
        expect(_bicicleta.id).toBe(Bicicleta.id);
        expect(_bicicleta.modelo).toBe(Bicicleta.modelo);
        expect(_bicicleta.color).toBe(Bicicleta.color);
        expect(_bicicleta.longitud).toBe(Bicicleta.longitud);
        expect(_bicicleta.latitud).toBe(Bicicleta.latitud);
        done();
    });

    it('test delete method', async (done) => {
        const _bicicleta = await Bicicletas.add(bicicleta);
        await expect(await Bicicletas.size()).toBe(1);
        await Bicicletas.delete(_bicicleta.id);
        expect(await Bicicletas.size()).toBe(0);
        done();
    });

    it('test update method', async (done) => {
        const _bicicleta = await Bicicletas.add(bicicleta);
        await Bicicletas.update(_bicicleta.id, {color: 'verde'});
        const {color} = await Bicicletas.show(_bicicleta.id)
        expect(color).toBe('verde');
        done();
    });

});
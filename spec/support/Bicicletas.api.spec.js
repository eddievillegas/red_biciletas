const axios = require('axios');
const db = require('../../db');
const server =  require('../../bin/www');
const Usuario = require('../../models/Usuario.model');
const Reserva = require('../../models/Reserva.model');
const Bicicleta = require('../../models/Bicicleta.model');
const Bicicletas = require('../../models/Bicicletas.model');
const { response } = require('express');

describe('API REST', () => {

    const url = 'http://localhost:3000/api/bicicletas';
    const bicicleta = Bicicleta.create({ modelo: 'x11', color: 'verde', latitud: -14.21344, longitud: 11.2232 });

    afterEach(async (done) => {
        await Bicicletas.empty()
        done();
    });

    it('GET api/bicicletas', async (done) => {
        const {data: {bicicletas: {length}} } = await axios.get(url);
        expect(length).toBe(0);
        done();
    });

    it('POST api/bicicletas', async (done) => {
        const {data: { bicicleta: _bicicleta }} = await axios.post(url, {bicicleta});
        expect(_bicicleta.color).toBe('verde');
        done();
    });

    it('GET api/bicicletas/:id', async (done) => {
        const {data: {bicicleta: bicicleta_1}} = await axios.post(url, {bicicleta});
        expect(bicicleta_1.color).toBe('verde');
        const {data: {bicicleta: bicicleta_2}} = await axios.get(`${url}/${bicicleta_1._id}`);
        expect(bicicleta_2.color).toBe('verde');
        done();
    });

    it('DELETE api/bicicletas/:id', async(done) => {
        const {data: {bicicleta: bicicleta_1}} = await axios.post(url, {bicicleta});
        expect(bicicleta_1.color).toBe('verde');
        const {data: {bicicleta: bicicleta_2}} = await axios.delete(`${url}/${bicicleta_1._id}`)
        expect(bicicleta_2.color).toBe('verde');
        done();
    });

    it('PUT api/bicicletas/:id', async(done) => {
        const {data: {bicicleta: bicicleta_1}} = await axios.post(url, {bicicleta});
        expect(bicicleta_1.color).toBe('verde');
        const {data: {bicicleta: bicicleta_2}} = await axios.put(`${url}/${bicicleta_1._id}`, {color: 'azul'});
        expect(bicicleta_2.color).toBe('azul')
        done();
    });

    // it('debe existir la reserva', async (done) => {
    //     try {
            
    //         const usuario = new Usuario({nombre: 'Ezequiel'});
    //         await usuario.save();
            
    //         const _bicicleta = new Bicicletas(bicicleta);
    //         await _bicicleta.save();

    //         const hoy = new Date();
    //         const manana = new Date();
            
    //         manana.setDate(hoy.getDate() + 1);

    //         await usuario.reserva(_bicicleta.id, hoy, manana);
            
    //         const reservas = await Reserva.find().populate({path: 'bicicleta'}).populate({path: 'usuario'}).lean().exec();
            
    //         const [reserva] = reservas;
            
    //         expect(reservas.length).toBe(1);
    //         expect(reserva.diasReserva).toBe(2);
    //         expect(reserva.bicicleta.id).toBe(_bicicleta.id);
    //         expect(reserva.usuario.nombre).toBe(usuario.nombre);
    //         expect(reservas.length).toBe(1);
            
    //         done();
    //     } catch (error) {
    //         console.error(error);
    //     }
    // });

});
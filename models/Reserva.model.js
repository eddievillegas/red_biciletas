const moment = require('moment');
const mongoose = require('mongoose');
const ReservaSchema = require('../Schemas/Reserva.schema');

ReservaSchema.methods.diasDeReserve = function(){
    return moment(this.hasta).diff(moment(this.desde), 'days') + 1;
}

module.exports = mongoose.model('Reserve', ReservaSchema);
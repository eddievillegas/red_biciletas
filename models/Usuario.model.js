const bcrypt = require('bcrypt');
const mongoose = require('mongoose');
const mailer = require('../models/Email.model');
const Reserva = require('../models/Reserva.model');
const UsuarioSchema = require('../Schemas/Usuario.schema');

UsuarioSchema.statics.index = function(){
    return this.find({});
}

UsuarioSchema.statics.add = function(usuario){
    const _usuario = new this(usuario);
    return _usuario.save();
}

UsuarioSchema.statics.delete = function(id){
    return this.deleteOne({_id:id});
}

UsuarioSchema.methods.reserva = function(biciId, desde, hasta) {
    const reserva = new Reserva({usuario: this._id, bicicleta: biciId, desde, hasta});
    return reserva.save();
}

UsuarioSchema.statics.send_email = function(mailOptions) {
    return mailer.sendMail(mailOptions);
}

UsuarioSchema.pre('save', function(next) {
    if(this.isModified('password'))
        this.password = bcrypt.hashSync(this.password, 12);
    next();
});

UsuarioSchema.methods.validPassword = function(password) {
    return bcrypt.compareSync(password, this.password);
}

module.exports =  mongoose.model('Usuario', UsuarioSchema);
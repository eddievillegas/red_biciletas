const mongoose = require('mongoose');
const TokenSchema =  require('../Schemas/Token.schema');
module.exports = mongoose.model('Token', TokenSchema);
const mongoose = require('mongoose');
const BicicletaSchema = require('../Schemas/Bicicleta.schema');

BicicletaSchema.statics.all = function(){
    return this.find({});
}

BicicletaSchema.statics.add = function(bicicleta){
    const _bicicleta = new this(bicicleta);
    return _bicicleta.save();
}

BicicletaSchema.statics.delete = function(id){
    return this.deleteOne({_id:id});
}

BicicletaSchema.statics.show = function(id){
    return this.findById(id);
}

BicicletaSchema.statics.update = function(id, bicicleta){
    return this.updateOne({_id:id}, bicicleta);
}

BicicletaSchema.statics.empty = function(id, bicicleta){
    return this.deleteMany({});
}

BicicletaSchema.statics.size = function(){  
    return this.countDocuments();
}

module.exports = mongoose.model('Bicicletas', BicicletaSchema);
const shortid = require('shortid');

class Bicicleta {

    constructor({color = '', modelo = '', latitud = 0, longitud = 0}) {
        this.color = color;
        this.modelo = modelo;
        this.latitud = latitud;
        this.longitud = longitud;
        this.ubicacion = [latitud, longitud];
    }

    static create(bicicleta){ return new Bicicleta(bicicleta); }

}

module.exports = Bicicleta;
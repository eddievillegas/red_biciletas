var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
const mongoose = require('mongoose');
const passport = require('./config/passport');
const session = require('express-session');
const jwt = require('jsonwebtoken');
require('dotenv').config()

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var tokensRouter = require('./routes/token');
var bicicletasRouter = require('./routes/bicicletas.route');
var bicicletasApiRouter = require('./routes/api/bicicletas.route');
const authApiRouter = require('./routes/api/auth.route');

const store = new session.MemoryStore();

var app = express();
app.use(session({
  store,
  resave: 'true',
  saveUninitialized: true,
  cookie: { maxAge: 240 * 60 * 60 * 1000 },
  secret: 'red_bicicletas_!!!****!!!!___==????'
}));

const mongoDB = process.env.MONGO_URI
mongoose.connect(mongoDB,{useNewUrlParser: true, useUnifiedTopology: true});

const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', () => console.log('connected'));

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');
app.set('secretKey', 'jwt_pwd_!!2223344');

app.use(logger('dev'));
app.use(express.json());
app.use(cookieParser());
app.use(passport.initialize());
app.use(passport.session());
app.use(express.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'public')));

app.get('/login', (req, res) => res.render('session/login'));

app.post('/login', (req, res, next) => {
  
  passport.authenticate('local', (error, user, info) => {

    if(error) return next(error);
    
    if(!user) return res.render('session/login', {info})
    
    req.logIn(user, err => {
      if(err) return next(err);
      res.redirect('/')
    });

  })(req, res, next);

});

app.get('/logout', (req, res) => {
  req.logout();
  res.redirect('/');
});

app.get('/forgotPassword', (req, res) => {
  res.render('session/forgotPassword');
})

app.post('/forgotPassword', (req, res) => {
  Usuario.findOne({email: req.body.email}, (error, usuario) => {
    if(!usuario) res.render('session/forgotPassword', {info: {message: 'valio verga'}});
    usuario.resetPassword(err => {
      if(err) next(err)
      console.log('session/forgotPasswordMessage') 
    });
    res.render('sessions/forgotPasswordMessage')
  })
});

const loggedIn = (req, res, next) => {
  if(req.user) next();
  else res.redirect('/login');
}

const validarUsuario = (req, res, next) => {
  jwt.verify(req.headers['x-access-token'], req.app.get('secretKey'), (error, decoded) => {
    if(error) {
      res.json({status: 'error', message: error.message, data: null});
    }
    else {
      req.body.userId = decoded.id;
      next();
    } 
  })
}

app.use('/', indexRouter);
app.use('/token', tokensRouter);
app.use('/usuarios', usersRouter);
app.use('/api/auth', authApiRouter);
app.use('/bicicletas',loggedIn, bicicletasRouter);
app.use('/api/bicicletas', validarUsuario, bicicletasApiRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;

const crypto = require('crypto');
const Token = require('../models/Token.model');
const UsuarioModel = require('../models/Usuario.model');

class UsuarioController {

    async index( req, res, next) {
        try {
            const usuarios = await UsuarioModel.find({});
            res.render('usuarios/index',{usuarios});
        } catch (error) {
            res.render('usuarios/index', {errors: error.errors});
        }
    }

    create( req, res, next ) {
        res.render('usuarios/create', {errors: {}});
    }
    
    async store(req, res, next){
        try {
            const usuario = await UsuarioModel.add(req.body);
            const token = new Token({_userId: usuario.id, token: crypto.randomBytes(16).toString('hex')});
            const mailOptions = {
                to: usuario.email,
                subject: 'Verificacion de cuenta',
                from: 'no-reply@redbicicletas.com',
                html: `<p>Hola para verificar su cuenta haga click en este link:</p><a href="http://localhost:3000/token/confirmation/${token.token}">Link</a>`,
            };
            await token.save();
            await UsuarioModel.send_email(mailOptions);
            res.redirect('/usuarios');
        } catch (error) {
            console.log(error)
            res.render('usuarios/create', {errors: error.errors});
        }
    }

    async delete(req, res, next){
        try {
            await UsuarioModel.delete(req.params.id);
            res.redirect('/usuarios');
        } catch (error) {
            res.render('usuarios/index', {errors: error.errors});
        }
    }

}

module.exports = new UsuarioController();
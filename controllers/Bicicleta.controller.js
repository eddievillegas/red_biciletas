const BicicletaModel = require('../models/Bicicleta.model');
const BicicletasModel = require('../models/Bicicletas.model');

class Bicicleta {
    
    async index(req, res, next) {
        try {
            res.render('bicicletas/index', {bicicletas: await BicicletasModel.all()});
        } catch (error) {
            res.render('bicicletas/index', {errors: error.errors});
        }
    }

    create(req, res, next){
        res.render('bicicletas/create');
    }

    async store(req, res, next) {
        try {
            await BicicletasModel.add(BicicletaModel.create(req.body));
            res.redirect('/bicicletas');
        } catch (error) {
            res.render('bicicletas/create', {errors: error.errors});
        }
    }

    async show(req, res, next){
        try {
            const bicicleta = BicicletasModel.show(req.params.id);
        } catch (error) {
            return res.json({bicicleta}).status(200);
        }
    }
    
    async edit(req, res, next){
        try {
            res.render('bicicletas/edit', {bicicleta: await BicicletasModel.show(req.params.id)});
        } catch (error) {
            res.render('bicicletas/edit', {errors: error.errors});
        }
    }

    async delete(req, res, next){
        try {
            await BicicletasModel.delete(req.params.id);
            res.redirect('/bicicletas');
        } catch (error) {
            res.render('bicicletas/index', {errors: error.errors});
        }
    }
    
    async update(req, res, next) {
        try {
            await BicicletasModel.update(req.body); 
            res.redirect('/bicicletas');
        } catch (error) {
            res.render('bicicletas/index', {errors: error.errors});
        }
    }

}

module.exports = new Bicicleta();
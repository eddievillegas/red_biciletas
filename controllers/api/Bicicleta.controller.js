const BicicletasModel = require('../../models/Bicicletas.model');

class Bicicleta {

  async index(req, res, next) {
    try {
      const bicicletas = await BicicletasModel.find();
      res.status(200).json({bicicletas});
    } catch (error) {
      res.status(400).json({error});
    }
  }

  async store(req, res, next) {
    try {
      res.json({bicicleta: await BicicletasModel.add(req.body.bicicleta)}).status(200);
    } catch (error) {
      res.json({error}).status(400);
    }
  }
  
  async show(req, res, next) {
    try {
      res.status(200).json({bicicleta: await BicicletasModel.show(req.params.id)});
    } catch (error) {
      res.status(400).json({error});
    }
  }
  
  async update(req, res, next) {
    const {id} = req.params
    try {
      await BicicletasModel.update(id, req.body); 
      res.status(200).json({bicicleta: await BicicletasModel.show(id)});
    } catch (error) {
      res.status(400).json({error});
    }
  }
  
  async destroy(req, res, next) {
    const {id} = req.params;
    try { 
      const bicicleta = await BicicletasModel.show(id);
      await BicicletasModel.delete(id);
      res.json({bicicleta}).status(200);
    } catch (error) {
      res.status(400).json({error});
    }
  }

}

module.exports = new Bicicleta();
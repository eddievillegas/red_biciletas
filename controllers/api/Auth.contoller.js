const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const Usuario = require('../../models/Usuario.model');


class AuthController {

    authenticate(req, res, next){
        Usuario.findOne({email: req.body.email}, (error, userInfo) => {
            if(error) {next(error)}
            else {
                if(userInfo ===  null) return res.status(401).json({status: 'error', message: 'Invalido email/password', data: null});
                if(userInfo != null && bcrypt.compareSync(req.body.password, userInfo.password)) {
                    const token = jwt.sign({id: userInfo._id}, req.app.get('secretKey'), {expiresIn: '7d' });
                    res.status(200).json({message: 'Usuario encontrado', data: {usuario: userInfo, token}});
                } else {
                    res.status(401).json({status: 'error', message: 'Invalido email/password', data: null});
                }
            }
        })        
    }

    forgotPassword(req, res, next) {
        Usuario.findOne({email: req.body.email}, (error, usuario) => {
            if(!usuario) return res.status(401).json({message: 'No existe el usuario', data: null});
            usuario.resetPassword(error => {
                if(error) return next(error)
                res.status(200).json({message: 'Se envio un email para restableceer el password', data: null});
            });
        });
    }

}

module.exports = new AuthController();
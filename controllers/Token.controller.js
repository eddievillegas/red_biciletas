const Token = require('../models/Token.model');
const UsuarioModel = require('../models/Usuario.model');

class TokenController {

    async confirmation(req, res, next) {
        try {
            const token = await Token.findOne({token: req.params.token});
            if(!token) res.status(400).send({msg: 'No se encotro el mensaje'});
            const usuario = await UsuarioModel.findById(token._userId);
            if(!usuario) res.status(400).send({msg: 'No encontramos un usuario'});
            usuario.verificado = true;
            await usuario.save();
            res.redirect('/')
        } catch (error) {
            if(error) res.status(500).send({msg: error.message});
        }
    }

}

module.exports = new TokenController();
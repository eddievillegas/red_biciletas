const passport = require('passport');
const Usuario = require('../models/Usuario.model');
const LocalStrategy = require('passport-local').Strategy

passport.use(new LocalStrategy({
    usernameField: 'email'
},(email, password, done) => {
    Usuario.findOne({ email }, (err, user) => {
        if (err) return done(err);
        if (!user) return done(null, false, { message: 'Email no existe o es incorrecto'});
        if (!user.validPassword(password)) return done(null, false, { message: 'Password incorrecto'});
        return done(null, user);
    });
  }
));

passport.serializeUser(function(user, cb){
    cb(null, user.id);
});

passport.deserializeUser(function(id, cb){
    Usuario.findById(id, function(err, usuario){
        cb(err, usuario);
    });
});

module.exports = passport;
const express = require('express');
const router = express.Router();
const bicicletaController = require('../../controllers/api/Bicicleta.controller');

router
    .get('/', bicicletaController.index)
    .post('/', bicicletaController.store)
    .get('/:id', bicicletaController.show)
    .put('/:id', bicicletaController.update)
    .delete('/:id', bicicletaController.destroy)

module.exports = router;
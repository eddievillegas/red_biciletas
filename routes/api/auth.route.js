const express = require('express');
const { route } = require('..');
const router = express.Router();

const authController = require('../../controllers/api/Auth.contoller');

router
    .post('/authenticate', authController.authenticate)
    .post('/forgotPassword', authController.forgotPassword);

module.exports = router
const express = require('express');
const BicicletaController = require('../controllers/Bicicleta.controller');
const router = express.Router();
const bicletaController = require('../controllers/Bicicleta.controller');

router
    .get('/', bicletaController.index)
    .post('/', bicletaController.store)
    .get('/:id/edit', bicletaController.edit)
    .post('/:id/delete', bicletaController.delete)
    .post('/:id/update', bicletaController.update)
    .get('/create', bicletaController.create)

module.exports = router;
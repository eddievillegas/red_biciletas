const express = require('express');
const { route } = require('.');
const router = express.Router();
const tokenController = require('../controllers/Token.controller');

router.get('/confirmation/:token', tokenController.confirmation);

module.exports = router;
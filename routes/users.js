const express = require('express');
const router = express.Router();
const usuarioController = require('../controllers/Usuario.controller');


router
  .get('/', usuarioController.index)
  .post('/', usuarioController.store)
  .get('/create', usuarioController.create)
  .post('/:id/delete', usuarioController.delete)

module.exports = router;

const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');

const validateEmail = (email) => {
    const re = /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/g;
    return re.test(email);
}

const UsuarioSchema = mongoose.Schema({
    nombre: {
        type: String,
        trim: true,
        required: [true, 'El nombre es obligatorio']
    },
    email:{
        type: String,
        trim: true,
        lowercase: true,
        unique: true,
        required: [true, "El email es obligatorio"],
        match: [/^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/g],
        validate: [validateEmail, "Por favor, ingresa un email valido"],
    },
    password: {
        type: String,
        required: [true, "El password es obligatorio"]
    },
    passwordResetToken: String,
    passwordResetTokenExpires: Date,
    verificado : {
        type: Boolean,
        default: false
    }
});

UsuarioSchema.plugin(uniqueValidator, { message: 'El (PATH) ya existe con otro usuario'});

module.exports = UsuarioSchema;
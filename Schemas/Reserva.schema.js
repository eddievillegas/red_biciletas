const mongoose = require('mongoose');

const ReservaSchema= mongoose.Schema({
    desde: Date,
    hasta: Date,
    bicicleta: { type: mongoose.Schema.Types.ObjectId, ref: 'Bicicleta' },
    usuario: { type: mongoose.Schema.Types.ObjectId, ref: 'Usuario'}
})

module.exports = ReservaSchema;
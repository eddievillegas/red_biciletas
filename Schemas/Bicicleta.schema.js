const mongoose = require('mongoose');

const BicicletaSchema = mongoose.Schema({
    modelo: String,
    color: String,
    ubicacion: { type: [Number], index: { type: '2dsphere', sparse: true } }
});

module.exports = BicicletaSchema;
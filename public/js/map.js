const map = L.map('main_map').setView([-34.6012424, -58.3861497], 13);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy: <a href="https://openstreetmap.org/copyright">OpenStreetmap</a> contributors'
}).addTo(map);

const setMap = async () => {
    const response = await fetch('api/bicicletas');
    const result = await response.json();
    result.bicicletas.forEach(bicicleta => L.marker(bicicleta.ubicacion, {title: bicicleta.id}).addTo(map));
}

setMap();

FROM node:15.0.1-alpine3.12
WORKDIR /app
COPY package*.json ./
COPY . /app
RUN ["npm", "install"]
EXPOSE 3000
CMD ["npm", "run", "devstart"]
